#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <dirent.h>

#define BACKLOG 10
#define BUFF_SIZE 10
#define NUM_CONSUMERS 10
#define MESSAGE_LEN 1024

/*
 Each thread will have a ThreadArg struct
 Each thread will take these "data packages" from a shared buffer and use these
    values to serve up correct page
 @param sock
 @param directory
   given to each thread to connect correct socket and serve up correct page
 @param count
 @param head
 @param tail
   keep track of location in shared buffer to read from
 @param notFull
 @param notEmpty
 @param mutex
   synchronizing access of shared buffer
*/
struct ThreadArg{
    int client_socket;
    int server_socket;
    
    char* directory;
    int port;
    
    int* count;
    int* head;
    int* tail;
    
    
    pthread_cond_t* notFull;
    pthread_cond_t* notEmpty;
    pthread_mutex_t* mutex;
};
typedef struct ThreadArg ThreadArg;

void recv_or_exit(int fd, char* buff, size_t max_len);
void send_or_exit(int fd, char* buff, size_t buff_len);
void* consume(void* param_args);
void *produce(void *arg);
int acceptConnections(int server_sock);
int createSocketAndListen(int port_num);


char* generateHTTPrequest(int sock, char* directory);
int isValidGETrequest(char* message);
char* receiveHTTPrequest(int sock);
char* generateHTML(char* directory_name);
void getRequstedFilename(char* http_request, char* filename);
char* listDirectoryContents(char* directory_name);

void sendHTTP200(int fd_200, char* filename, char *directory_name);
void sendHTTP400(int fd_400);
void sendHTTP404(int fd_404);

int fileExists(char* filename, char * directory_name);
int isFileDirectory(const char* path, char* directory_name);
int containsIndexHTML(char* directory_name);

void printSharedBuffer();


/*Set up shared buffer.
 *  *Threads will read from these cells to operate in parallel on serving up file */

//Global variables
ThreadArg* shared_buffer;
int buff_count = 0;
int buff_head = 0;
int buff_tail = 0;


int main(int argc, char** argv) {
    /* Check command line args */
    shared_buffer = malloc(sizeof(ThreadArg)*BUFF_SIZE);
    if (argc != 3){
        printf("\n\n");
        printf("Usage: ./toreroserve port directory\n");
        printf("\t port: the port number the server will listen on for incoming connections \n");
        printf("\t directory: the directory out of which the server will serve files\n");
        printf("\n\n");
        exit(1);
    }
    /* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> VARIABLE SET UP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    /* Grab command line args */
    int port = (int)strtol(argv[1], NULL, 10);
    char* directory = argv[2];
    memset(shared_buffer, 0, sizeof(ThreadArg)*BUFF_SIZE);
    
        
    /*Create and initialize condition variables*/
    pthread_cond_t notEmpty;
    pthread_cond_t notFull;
    pthread_mutex_t mutex;
    pthread_cond_init(&notEmpty, NULL);
    pthread_cond_init(&notFull, NULL);
    pthread_mutex_init(&mutex, NULL);
    
    /*Create ThreadArg instance and link attributes to variables in main*/
    int server_socket = createSocketAndListen(port);
    
    ThreadArg thread_cell;
    thread_cell.count = &buff_count;
    thread_cell.head = &buff_head;
    thread_cell.tail = &buff_tail;
    thread_cell.server_socket = server_socket;
    thread_cell.client_socket = -1;
    thread_cell.directory = directory;
    thread_cell.port = port;
    thread_cell.mutex = &mutex;
    thread_cell.notEmpty =&notEmpty;
    thread_cell.notFull = &notFull;
    
    
    /*One producer to transfer command line args. Many consumers (threads) to do work*/
    pthread_t producer;
    pthread_t consumers[NUM_CONSUMERS];
    
    /*Allocate memory for Thread args*/
    ThreadArg *args = malloc((NUM_CONSUMERS+1)*sizeof(ThreadArg));
    if(args == NULL){
        printf("malloc() failed for ThreadArg args \n");
        exit(1);
    }
    
    /*Allocate memory for threads*/
    pthread_t* threads = malloc((NUM_CONSUMERS+1)*sizeof(pthread_t));
    if(threads == NULL){
        printf("malloc() failed for pthread_t threads \n");
        exit(1);
    }
    
    /* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BEGIN SERVE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    
    /*Create new socket.
     *Start listening for new connections on specified port*/
    
     while(1){
        /*Create producer thread*/
        pthread_create(&producer, NULL, produce, (void*)&thread_cell);
        pthread_join(producer, NULL); /*Wait for producer to finish*/
        
        /*Create a pool of consumer threads*/
        int i;
        for (i = 0; i < NUM_CONSUMERS; ++i) {
            pthread_create(&consumers[i], NULL, consume, (void*)&thread_cell);
            pthread_detach(consumers[i]);//let the consumers run without us waiting to join with them
        }
    }
    
    /* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CLEAN UP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    close(server_socket);
    free(args);
    free(threads);
    pthread_cond_destroy(&notEmpty);
    pthread_cond_destroy(&notFull);
    pthread_mutex_destroy(&mutex);
    
    return 0;
}

/*
 * The server uses the producer/consumer model to handle requests.
 * Threads access a shared buffer - access must be synchronized
 * Producers place HTTP requests in the buffer
 */
void *produce(void *param_args) {
    //Unpack thread arg struct
    ThreadArg* producer_thread_args = (ThreadArg*)param_args;
    //Lock section
    pthread_mutex_lock(producer_thread_args->mutex);
     
    //Wait for space to be available in buffer
    while(*producer_thread_args->count == BUFF_SIZE){
        pthread_cond_wait(producer_thread_args->notFull, producer_thread_args->mutex);
    }
    
    //Accepts first waiting connection on server_socket. Sets "client_sock" in producer_thread_args
    int server_socket = (producer_thread_args->server_socket);
    int client_socket =  acceptConnections(server_socket);
    
    //Put data in buffer
    producer_thread_args->client_socket = client_socket;
    shared_buffer[ *producer_thread_args-> tail] = *producer_thread_args;
    
    //Update tail pointer
    *producer_thread_args->tail = (*producer_thread_args->tail + 1) % BUFF_SIZE;
    (*producer_thread_args->count)++;
    
    
    //Signal that buffer is not empty
    pthread_cond_signal(producer_thread_args->notEmpty);
    //Unlock section
    pthread_mutex_unlock(producer_thread_args->mutex);
    
    return NULL;
}

/*
 * The server uses the producer/consumer model to handle requests.
 * Threads access a shared buffer - access must be synchronized
 * Consumers handle HTTP requests in the buffer
 */
void* consume(void* param_args){
    //Extract args
    ThreadArg* current_thread_args = (ThreadArg*)param_args;
    
    /*---------------------------------<Synchronize>----------------------------------------*/
    /*------------------------------------------------------------------------------------*/
    //Lock sectionn
    pthread_mutex_lock(current_thread_args->mutex);
    
    //Wait for items to be put in buffer
    while((*current_thread_args->count) == 0){
        pthread_cond_wait(current_thread_args->notEmpty, current_thread_args->mutex);
    }
    
    //Extract data from buffer
    int client_socket = (shared_buffer[*current_thread_args->head].client_socket);
    char* directory = shared_buffer[*current_thread_args->head].directory;
    
    //Update buffer position pointers
    *current_thread_args->head = (*current_thread_args->head + 1) % BUFF_SIZE;
    (*current_thread_args->count)--;
    
    //Signal threads that buffer has 1 less item
    pthread_cond_signal(current_thread_args->notFull);
    
    //Unlock section
    pthread_mutex_unlock(current_thread_args->mutex);
    /*------------------------------------</Synchronize>-------------------------------------*/
    /*------------------------------------------------------------------------------------*/
    
    //Get HTTP request from client 
    char* directory_name = directory;
    char filename[100];
    char* http_request = receiveHTTPrequest(client_socket);
    
    //Handle request   
    if(isValidGETrequest(http_request)){
        getRequstedFilename(http_request, filename);
        //FILE EXISTS
        if(fileExists(filename, directory_name) == 0){
            //CASE - FILE - OK
            if(isFileDirectory(filename, directory_name) == 0){
                sendHTTP200(client_socket, filename, directory_name);
            }
            //CASE - directory - NOK
            else{
                char* dir_to_search = malloc(1024*sizeof(char));//create full path to send to function "contains index.html"
                dir_to_search = strcat(dir_to_search, "WWW");
                dir_to_search = strcat(dir_to_search, filename);
                
                //CASE - CONTANS index.html - serve index.html
                //eg /test/dir/ --------------- serve /test/dir/index.html
                if(containsIndexHTML(dir_to_search)){
                    char* dir_with_indexhtml = malloc(1024*sizeof(char)); //provided_dir_name/index.html
                    dir_with_indexhtml = strcat(dir_with_indexhtml, filename);
                    dir_with_indexhtml = strcat(dir_with_indexhtml, "index.html");
                    sendHTTP200(client_socket, dir_with_indexhtml, directory_name);
                }
                
                //CASE - DOES NOT CONTAIN index.html - display webpage with link to files in directory
                else{
                    generateHTML(dir_to_search);
                    char* file_to_search = malloc(1024*sizeof(char));
                    strcpy(file_to_search, dir_to_search);
                    file_to_search = strcat(file_to_search, "index.html");
                    sendHTTP200(client_socket, file_to_search, "");
                    remove(file_to_search);
                }
            }
        }
        //CASE - File does not exist
        else{
            sendHTTP404(client_socket);
        }
    }
    //CASE - invalid http request
    else{
        sendHTTP400(client_socket);
    }
    return NULL;
}


/*
 * Creates a new socket and starts listening on that socket for new
 * connections.
 *
 *@param port_num The port number on which to listen for connections.
 *@returns The socket file descriptor
 */
int createSocketAndListen(int port_num) {
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0) {
        perror("Creating socket failed");
        exit(1);
    }
    int reuse_true = 1;
    int retval;
    retval = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuse_true,sizeof(reuse_true));
    if (retval < 0) {
        perror("Setting socket option failed");
        exit(1);
    }
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port_num);
    addr.sin_addr.s_addr = INADDR_ANY;
    
    retval = bind(sock, (struct sockaddr*)&addr, sizeof(addr));
    if(retval < 0) {
        perror("Error binding to port");
        exit(1);
    }
    retval = listen(sock, BACKLOG);
    if(retval < 0) {
        perror("Error listening for connections");
        exit(1);
    }
    return sock;
}
/*
 *Sit around forever accepting new connections from client.
 *@param server_sock The socket used by the server.
 *Sets client socket value. producer will now have access to this
 */
int acceptConnections(int server_sock) {
    int sock;
    struct sockaddr_in remote_addr;
    unsigned int socklen = sizeof(remote_addr);
    sock = accept(server_sock, (struct sockaddr*) &remote_addr, &socklen);
    if(sock < 0) {
        perror("Error accepting connection");
        exit(1);
    }
    return sock;
}


/*
 * Validates the format of the received HTTP request
 */
int isValidGETrequest(char *request_line){
    char local_request[strlen(request_line)];
    strcpy(local_request,request_line);
    char *token = strtok(local_request, " ");
    if(token == NULL ||strcmp(token, "GET") != 0){
        return 0;
    }
    token = strtok(NULL, " ");
    if(token == NULL || (token[0] != '/') || !((strrchr(token, '.') != NULL) || (token[strlen(token) - 1] == '/'))){
        return 0;
    }
    token = strtok(NULL, "/");
    if(token == NULL || strcmp(token, "HTTP") != 0){
        return 0;
    }
    token = strtok(NULL, "\n");
    if(token == NULL || (strlen(token) != 4) || (token[3] != '\r') || (token[1] != '.')){
        return 0;
    }
    if(token == NULL || !((token[0] == '1' && token[2] == '0') || (token[0] == '2' && token[2] == '0') || (token[0] == '1' && token[2] == '1'))){
        return 0;
    }
    return 1;
}

/*
 * Allocates a buffer for the received message and calls recv_or_exit to fill the allocated buffer
 * @return buffer filled with received message
 */
char* receiveHTTPrequest(int sock){
    char* http_request = malloc(MESSAGE_LEN);
    recv_or_exit(sock, http_request, MESSAGE_LEN); 
    return http_request;
}

/*
 * Extract the requested filename from the http request
 */
void getRequstedFilename(char* http_request, char* filename) {
    char *token = strtok(http_request, " ");
    token = strtok(NULL, " ");
    strcpy(filename,token);
}

/*
 * Opens and searches the directory pointed to by directory_name 
 * @return list of all files found in the directory
 *
 */
char* listDirectoryContents(char* directory_name){
    char* files_in_dir = malloc(99999*sizeof(char));
    DIR* dirp = opendir(directory_name);
    struct dirent* dp;
    
    if(dirp){
        //readdir() function returns a pointer to the next directory entry
        while((dp = readdir(dirp)) != NULL){
            files_in_dir = strcat(files_in_dir, dp->d_name);
            files_in_dir = strcat(files_in_dir, " ");
        }
    }
    closedir(dirp);
    return files_in_dir;
}


//Called when the requested directory does not contain index.html - displays webpage with link to files in directory
char* generateHTML(char* folder){
    FILE* html_gen;
    char* html = malloc(sizeof(char)*99999);
    char* address = malloc(sizeof(char)*99999);
    
    DIR* d;
    struct dirent *dir;
    d = opendir(folder);
    
    strcpy(address, folder);
    strcat(address, "/index.html");
    
    
    strcat(html, "<html><head><title>");
    strcat(html, folder);
    strcat(html,"</title></head>\n</body>");
    strcat(html, "<ul style=\"list-style-type:none\">");
    strcat(html, "<li>Index of: ");
    strcat(html, folder);
    strcat(html, "</li>\n");
    
    if(d){
        while((dir=readdir(d)) != NULL){
            strcat(html, "<li><a href=\"");
            //strcat(html, folder);
            strcat(html, dir->d_name);
            strcat(html, "\" >");
            strcat(html, dir->d_name);
            strcat(html, "</a></li>\n");
        }
        closedir(d);
    }
    
    strcat(html, "</ul>\n</body>\n</html>");
    
    
    html_gen = fopen(address, "w+");
    if(!html_gen){
        exit(1);
    }
    chmod(address, S_IRWXU|S_IRWXG|S_IRWXO);
    
    fputs(html, html_gen);
    fclose(html_gen);
    
    return html;
}

//Send HTTP 400 Bad Request
void sendHTTP400(int fd_400){  
    char buff[MESSAGE_LEN];
    memset(buff, 0, MESSAGE_LEN);
    char *response_400 = "HTTP/1.1 400 Bad Request\r\nContent-Type:text/html\r\n\r\n<html>\n<body>\n<h1><strong>400 Error: Bad request</strong></h1>\n<p>Your browser sent a request that this server could not understand.</p>\n</body></html>\n\r\n";
    strcpy(buff, response_400);
    send_or_exit(fd_400, buff, MESSAGE_LEN);
    memset(buff, 0, MESSAGE_LEN);
    
}

//Send HTTP 404 Not Found
void sendHTTP404(int fd_404){
    char buff[MESSAGE_LEN];
    memset(buff, 0, MESSAGE_LEN);
    char *response_404 = "HTTP/1.1 404 Not Found\r\nContent-Type:text/html\r\n\r\n<html>\n<body>\n<h1><strong>404 Error: File Not Found</strong></h1>\n<p>The URL you requested was not found on this server.</p>\n</body></html>\n\r\n";
    strcpy(buff, response_404);
    send_or_exit(fd_404, buff, MESSAGE_LEN);
    memset(buff, 0, MESSAGE_LEN);
}


//Send HTTP200 OK
void sendHTTP200(int fd_200, char* filename, char *directory_name){
    FILE *requested_file;
    int length;
    char local_filename[100];
    char full_path[1000];
    char length_as_string[1000];
    char header[1000];
    char *body;
    char media_type_line[1000];
    //char buff[MESSAGE_LEN];
    
    strcpy(local_filename, filename);
    //merge the root directory (WWW) with the requested file path
    strcpy(full_path,directory_name);
    strcat(full_path, local_filename);
    
    requested_file = fopen(full_path, "rb");  //open the file
    if (!requested_file){
        return;
    }
    
    //RESPONSE
    strcat(header, "HTTP/1.0 200 OK\r\n");
    
    //*******************CONTENT LENGTH LINE*********************//
    //Calculate the length of the file
    fseek(requested_file, 0, SEEK_END);
    length=ftell(requested_file);
    fseek(requested_file, 0, SEEK_SET);
    
    
    sprintf(length_as_string, "%d", length); //append Content length line
    strcat(header,"Content-Length: ");
    strcat(header,length_as_string);
    strcat(header,"\r\n");
    
    //MEDIA TYPE LINE
    if(isFileDirectory(local_filename, directory_name)){
        strcat(media_type_line,"Content-Type: text/html\r\n");
    }
    char *extention = strtok(local_filename, ".");
    extention = strtok(NULL, "\n");
    if(strcmp(extention, "html") == 0){
        strcat(media_type_line,"Content-Type: text/html\r\n");
    }
    else if(strcmp(extention, "txt") == 0){
        strcat(media_type_line,"Content-Type: text/html\r\n");
    }
    else if(strcmp(extention, "jpeg") == 0)
    {
        strcat(media_type_line,"Content-Type: image/jpeg\r\n");
    }
    else if(strcmp(extention, "gif") == 0)
    {
        strcat(media_type_line,"Content-Type: image/gif\r\n");
    }
    else if(strcmp(extention, "png") == 0)
    {
        strcat(media_type_line,"Content-Type: image/png\r\n");
    }
    else if(strcmp(extention, "pdf") == 0)
    {
        strcat(media_type_line,"Content-Type: application/pdf\r\n");
    }
    strcat(header,media_type_line);
    strcat(header,"\r\n");
    
    //*******************************BODY********************************/
    
    body=(char *)malloc(length+1);
    if (!body){
        fprintf(stderr, "Malloc error");
        fclose(requested_file);
        return;
    }
    
    //Read file contents into buffer
    fread(body, length, 1, requested_file);
    fclose(requested_file);
    
    int response_length = strlen(header)+length;
    char *response = (char*)malloc(response_length);
    strcpy(response, header);
    memcpy(response+strlen(header), body, length);
    free(body);
           
    send_or_exit(fd_200,response,response_length);
    
    free(response);
}



/*
 * Uses the access system call to check if the file specified by directory_name/filename exists
 * @return: 0 if file exists, else -1
 */
int fileExists(char* filename, char * directory_name){
   int result;
   char full_path[1000];
   strcpy(full_path,directory_name);
   strcat(full_path, filename);
    
    //The access() system call checks the accessibility of the file named by the path argument for the access permissions indicated by the mode argument
   result = access(full_path, F_OK);
   return result;
}

/*
 * This function uses the stat struct and S_ISDIR macro to test if the referenced file is a directory
 * @return nonzero if file is a directory
 */
int isFileDirectory(const char* path, char* directory_name){   
    char full_path[1000];
    strcpy(full_path,directory_name);
    strcat(full_path, path);
    
    //The stat utility displays information about the file pointed to by file
    struct stat stat_path;
    stat(full_path, &stat_path);
    //The file mode, stored in the st_mode field of the file attributes, contains two kinds of information: the file type code, and the access permission bits. S_ISDIR returns non-zero if the file is a directory
    return S_ISDIR(stat_path.st_mode);
}    


/*
 * Searches the directory specified by directory_name for an index.html file
 * @return: 1 if index.html is found, else 0
 */
int containsIndexHTML(char* directory_name){
    DIR* dirp = opendir(directory_name);
    struct dirent* dp;
    if(dirp){
        while((dp = readdir(dirp)) != NULL){
            if(strcmp(dp->d_name, "index.html")== 0){
                return 1;
            }
        }
    }
    closedir(dirp);
    return 0;
}

/*
 * Responsible for sending an http request and exiting on error
 * Transmits the message in buff to the socket described by fd
 * Called from: sendHTTP400, sendHTTP404, sendHTTP200
 *
 * @param: fd: the socket to send the message to
 * @param buff: the message to send
 * @param buff_len: the length of the buffer containing the message
 */
void send_or_exit(int fd, char* buff, size_t buff_len){
    int sent = send(fd, buff, buff_len, 0);
    if(sent == 0){
        printf("Server connection closed unexpectedly. Good bye");
        exit(1);
    }
    else if(sent == -1){
        perror("send");
        exit(1);
    }
}

/*
 * Responsible for receiving an http request
 *
 * @param fd:
 * @param buff: source address of the message to be filled in with received content
 * @param max_len: length of buffer
 * @return: none. buff has been filled with the bytes received on fd
 */
void recv_or_exit(int fd, char* buff, size_t max_len){
    int recvd = recv(fd, buff, max_len, 0);
    
    //recv returns the length of the message on successful completion
    if(recvd == 0){
        printf("Server connection closed unexpectedly. Good bye. \n");
        exit(1);
    }
    
    else if(recvd == -1){
        perror("recv");
        exit(1);
    }
}

//DEBUG: Prints the contents of the shared buffer - for debugging purposes
void printSharedBuffer(){
    int i = 0;
    while(i < buff_tail){
        printf("PRINTING BUFFER:... \n");
        printf("Cell %d... \n", i);
        printf("\tClient socket: %d\n", (shared_buffer[i].client_socket));
        printf("\tClient socket at %p \n",&shared_buffer[i].client_socket);
        printf("\tServer socket: %d\n", (shared_buffer[i].server_socket));
        printf("\tDirectory: %s\n", (shared_buffer[i].directory));
        printf("\tPort:%d\n", (shared_buffer[i].port));
        printf("\tHead: %d\n", *(shared_buffer[i].head));
        printf("\tTail: %d\n", *(shared_buffer[i].tail));
        printf("\tCount: %d\n", *(shared_buffer[i].count));
        printf("\n");
        i++;
    }
}



