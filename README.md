# ToreroServe 
### A Lean Webserver

This project is a web server that serves up pages from a WWW directory. The server is multithreaded with pthreads and can handle multiple connections currently. The server can serve files with html, txt, jpeg, gif, png and pdf extensions. If the server receives a request for a directory, it responds by serving the “index.html” file inside that directory if one is present, or displaying a list of files in that directory if no “index.html” file is present. If the server receives a request  for a file that does not exist, it responds with a 404 Not Found page. If the server receives a improperly formatted HTTP request, it responds with a 400 Bad Request page.

From this project, I gained experience in using HTTP to transfer files to web clients, using multithreading to concurrently handle multiple connections, and using sockets to communicate via TCP. 